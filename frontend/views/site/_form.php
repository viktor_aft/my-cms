<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var backend\models\ProductRequest $model */
/** @var common\classes\ProductSearch $products */
/** @var yii\widgets\ActiveForm $form */

$productList = ArrayHelper::map($products, 'id', 'name');
?>

<div class="product-request-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'customer_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'product_id')->dropDownList($productList, ['prompt' => 'Укажите продукцию']) ?>

    <div class="form-group" style="padding-top: 10px">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
