<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var backend\models\ProductRequest $model */
/** @var common\classes\ProductSearch $products */

$this->title = 'Создать запрос';

?>
<?php if(Yii::$app->session->hasFlash('success')){?>
    <div style="text-align: center;font-size: 25px;font-weight: bold;color: green;"><?=Yii::$app->session->hasFlash('success')?></div>
<?php }?>

<div class="product-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'products' => $products,
    ]) ?>

</div>
