<?php

use yii\db\Migration;

/**
 * Class m230523_095002_add_data
 */
class m230523_095002_add_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('product', ['name' => 'Яблоки', 'price' => 100]);
        $this->insert('product', ['name' => 'Апельсины', 'price' => 150]);
        $this->insert('product', ['name' => 'Мандарины', 'price' => 200]);

        $this->insert('request_status', ['name' => 'Принята']);
        $this->insert('request_status', ['name' => 'Отказана']);
        $this->insert('request_status', ['name' => 'Брак']);


        $this->insert('log_event', ['name' => 'Создана']);
        $this->insert('log_event', ['name' => 'Изменена']);
        $this->insert('log_event', ['name' => 'Удалена']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m230523_095002_add_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230523_095002_add_data cannot be reverted.\n";

        return false;
    }
    */
}
