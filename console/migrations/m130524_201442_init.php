<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'price' => $this->float()->notNull(),
            'is_active' => $this->boolean()->notNull()->defaultValue(true),
            'is_deleted' => $this->boolean()->notNull()->defaultValue(false),
        ], $tableOptions);

        $this->createTable('request_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createTable('product_request', [
            'id' => $this->primaryKey(),
            'request_name' => $this->string()->notNull(),
            'customer_name' => $this->string()->notNull(),
            'phone' => $this->string()->notNull()->unique(),
            'comment' => $this->text(),
            'product_id' => $this->integer()->notNull(),
            'status_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull()->defaultValue(time()),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex(
            'idx-product_request-product_id',
            'product_request',
            'product_id'
        );

        $this->addForeignKey(
            'fk-product_request-product_id',
            'product_request',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-product_request-status_id',
            'product_request',
            'status_id'
        );

        $this->addForeignKey(
            'fk-product_request-status_id',
            'product_request',
            'status_id',
            'request_status',
            'id',
            'CASCADE'
        );

        $this->createTable('log_event', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createTable('request_log', [
            'id' => $this->primaryKey(),
            'request_id' => $this->integer()->notNull(),
            'event_id' => $this->integer()->notNull(),
            'field' => $this->string(),
            'old_value' => $this->string(),
            'new_value' => $this->string(),
            'created_at' => $this->integer()->notNull()->defaultValue(time()),
        ], $tableOptions);

        $this->createIndex(
            'idx-request_log-request_id',
            'request_log',
            'request_id'
        );

        $this->addForeignKey(
            'fk-request_log-request_id',
            'request_log',
            'request_id',
            'product_request',
            'id',
            'CASCADE',
        );

        $this->createIndex(
            'idx-request_log-event_id',
            'request_log',
            'event_id'
        );

        $this->addForeignKey(
            'fk-request_log-event_id',
            'request_log',
            'event_id',
            'log_event',
            'id',
            'CASCADE',
        );
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
        $this->dropTable('request_log');
        $this->dropTable('log_event');
        $this->dropTable('product_request');
        $this->dropTable('request_status');
        $this->dropTable('product');
    }
}
