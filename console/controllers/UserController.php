<?php

namespace console\controllers;

use backend\modules\cms\models\User;
use yii\console\Controller;

class UserController extends Controller
{
    public function actionCreateAdmin(){
        $user = new User();
        $user->username = 'admin';
        $user->status = User::STATUS_ACTIVE;
        $user->setPassword('123456');
        $user->email = 'admin@test.ru';
        $user->generateAuthKey();
        $user->save();
    }

    public function actionCreateManager(){
        $user = new User();
        $user->setPassword('123456');
        $user->status = User::STATUS_ACTIVE;
        $user->username = 'manager';
        $user->generateAuthKey();
        $user->email = 'manager@test.ru';
        $user->save();
    }

}