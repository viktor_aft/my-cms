<?php

use backend\modules\cms\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var User $model */
/** @var yii\widgets\ActiveForm $form */

$statuses = [
        User::STATUS_ACTIVE => 'Активен',
        User::STATUS_INACTIVE => 'Деактивирован',
        User::STATUS_DELETED => 'Удален',
];
$roles = Yii::$app->authManager->getRoles();
$userRole = Yii::$app->authManager->getRolesByUser($model->id);
$roleList = ArrayHelper::map($roles, 'name', 'description');
$paramStatus = ['options' => [ $model->status => ['Selected' => true]], 'prompt' => 'Укажите статус'];

?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput() ?>
    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'status')->dropDownList($statuses, $paramStatus) ?>
    <div style="padding-top: 10px"><?= Html::dropDownList('group', $userRole[array_key_first($userRole)]->name, $roleList, ['prompt' => 'Укажите группу'])?></div>


    <div class="form-group" style="padding-top: 10px">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
