<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <p><a class="btn btn-outline-secondary" href="/cms/user/">Список пользователей</a></p>
            </div>
            <div class="col-lg-4">
                <p><a class="btn btn-outline-secondary" href="/request-log/">Логи запросов</a></p>
            </div>
            <div class="col-lg-4">
                <p><a class="btn btn-outline-secondary" href="/product-request/">Запросы товаров</a></p>
            </div>
        </div>

    </div>
</div>
