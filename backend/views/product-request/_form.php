<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var backend\models\ProductRequest $model */
/** @var common\classes\ProductSearch $products */
/** @var \common\classes\RequestStatusSearch $statuses */
/** @var yii\widgets\ActiveForm $form */

$productList = ArrayHelper::map($products, 'id', 'name');
$statusList = ArrayHelper::map($statuses, 'id', 'name');
$paramProduct = ['options' => [ $model->product_id => ['Selected' => true]], 'prompt' => 'Укажите продукцию'];
$paramStatus = ['options' => [ $model->status_id => ['Selected' => true]], 'prompt' => 'Укажите статус'];
?>

<div class="product-request-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'request_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customer_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'product_id')->dropDownList($productList, $paramProduct) ?>

    <?= $form->field($model, 'status_id')->dropDownList($statusList, $paramStatus) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
