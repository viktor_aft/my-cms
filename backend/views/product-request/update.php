<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var backend\models\ProductRequest $model */
/** @var backend\classes\ProductRequestSearch $products */
/** @var \common\classes\RequestStatusSearch $statuses */

$this->title = 'Обновить заявку: ' . $model->id;
?>
<div class="product-request-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'products' => $products,
        'statuses' => $statuses,
    ]) ?>

</div>
