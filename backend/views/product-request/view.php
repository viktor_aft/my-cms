<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var backend\models\ProductRequest $model */
/** @var common\classes\ProductSearch $product */
/** @var common\classes\RequestStatusSearch $status */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-request-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'request_name',
            'customer_name',
            'phone',
            'comment:ntext',
            [
                'label' => 'Продукция',
                'value' => $product->name,
            ],
            [
                'label' => 'Статус',
                'value' => $status->name,
            ],
        ],
    ]) ?>

</div>
