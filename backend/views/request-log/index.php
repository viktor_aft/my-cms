<?php

use backend\models\RequestLog;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Request Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-log-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'request_id',
            'event_id',
            'field',
            'old_value',
            'new_value',
            'created_at:datetime',
        ],
    ]); ?>


</div>
