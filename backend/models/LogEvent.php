<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "log_event".
 *
 * @property int $id
 * @property string $name
 *
 * @property RequestLog[] $requestLogs
 */
class LogEvent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_event';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[RequestLogs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestLogs()
    {
        return $this->hasMany(RequestLog::class, ['event_id' => 'id']);
    }
}
