<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "request_log".
 *
 * @property int $id
 * @property int $request_id
 * @property int $event_id
 * @property string $field
 * @property string $old_value
 * @property string $new_value
 * @property int $created_at
 *
 * @property LogEvent $event
 * @property ProductRequest $request
 */
class RequestLog extends \yii\db\ActiveRecord
{
    const STATUS_CREATE = 1;
    const STATUS_EDIT = 2;
    const STATUS_DELETE = 3;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'request_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'event_id'], 'required'],
            [['request_id', 'event_id', 'created_at'], 'integer'],
            [['field', 'old_value', 'new_value'], 'string', 'max' => 255],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogEvent::class, 'targetAttribute' => ['event_id' => 'id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductRequest::class, 'targetAttribute' => ['request_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'request_id' => 'Request ID',
            'event_id' => 'Event ID',
            'field' => 'Field',
            'old_value' => 'Old Value',
            'new_value' => 'New Value',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Event]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(LogEvent::class, ['id' => 'event_id']);
    }

    /**
     * Gets query for [[Request]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(ProductRequest::class, ['id' => 'request_id']);
    }
}
