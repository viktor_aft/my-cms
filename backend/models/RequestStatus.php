<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "request_status".
 *
 * @property int $id
 * @property string $name
 *
 * @property ProductRequest[] $productRequests
 */
class RequestStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'request_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[ProductRequests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductRequests()
    {
        return $this->hasMany(ProductRequest::class, ['status_id' => 'id']);
    }
}
